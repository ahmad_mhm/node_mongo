const express = require('express'),
 path = require('path'),
 mongoose = require('mongoose'),
 config = require('./config/database'),
 bodyParser = require('body-parser'),
 session = require('express-session'),
    expressValidator = require('express-validator'),
    router = require('./routes/index')

require('dotenv').config()


//connect to database
mongoose.connect(config.database)
let db = mongoose.connection

db.on('error', console.error.bind('console', 'connection error:'))
db.once('open', function (){
    console.log('connected to mongo db')
})
//initializing
let app = express()


app.set('views', path.join(__dirname, 'resources/views'))
app.set('view engine', 'ejs')

//set public directory
app.use(express.static(path.join(__dirname, 'public')))

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: true }
}))
app.post(
    '/user',
    // username must be an email
    expressValidator.body('username').isEmail(),
    // password must be at least 5 chars long
    expressValidator.body('password').isLength({ min: 5 }),
    (req, res) => {
        // Finds the validation errors in this request and wraps them in an object with handy functions
        const errors = expressValidator.validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({ errors: errors.array() });
        }

        User.create({
            username: req.body.username,
            password: req.body.password,
        }).then(user => res.json(user));
    },
);

app.use(require('connect-flash')());
app.use(function (req, res, next) {
    res.locals.messages = require('express-messages')(req, res);
    next();
});
/*app.get('/', function (req, res){
    res.render('admin/layouts/master')
})*/
app.use('/', router)
//initializing the server
let port = 3000
app.listen(port, function (){
    console.log('server started on port :' + port)
})


