const express = require('express')
require('express-group-routes');
const router = express.Router()

// user routes
require('./admin/web.router')(router)
require('./front/web.router')(router)

// admin routes

module.exports = router