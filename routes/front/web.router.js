const home = require('../../app/controllers/front/home.ctrl')

module.exports = function (router) {
    router.get('/', home.index)
}