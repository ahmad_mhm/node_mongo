const home = require('../../app/controllers/admin/home.ctrl')
const adminPrefix = 'admin'
module.exports = function (router) {
    //router.use(`\${adminPrefix}`, [ adminMiddleware ]);
    router.group(`/${adminPrefix}`, (router) => {
        router.get('/', home.index)
    });
    //router.get('/admin', home.index)
}